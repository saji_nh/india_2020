require 'json'
require 'pp'
require_relative '../helper_libs.rb'

arr=""
File.open("india_popln.json") do |file|

  arr = JSON.load(file)
end
sarr = []
arr["data"][1..-1].each {|arr| sarr << {:name=>make_slug(arr[1]),
                         :population=>arr[2], :pop_density=>arr[4]}}
sorted_sarr= sarr.sort_by {|hsh| hsh[:name] }

fout = File.open("population_stats.csv","w")
fout.puts "state_name, population, population_density"
sorted_sarr.each do |hsh|
  fout.puts hsh.values.join(", ")
end
fout.close

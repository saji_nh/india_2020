require 'fileutils'
# write pop stats to the ByState directory for each state

outdir = "../ByState/Popln"
FileUtils.mkdir_p(outdir)

pop_stats = IO.readlines("./india_pop_stats.csv")

pop_stats[1..-1].each do |row|
  row.chomp!
  arr=row.split(",")
  File.open(File.join(outdir,arr[0])+".csv","w") do |fout| 
    fout.puts arr[1..2].join(",").strip
  end
end

require 'json'

def get_vacc_data
  arr=""
  File.open("vdata.05-08-2021") do |file|

    arr = JSON.load(file)
  end
  arr['vaccine_data']
end
# each record of vacc_data  contains
# under 'vaccine_data' a hash with important keys as
# "day" and "vacc_st_data"
#  day is in 2021-mm-dd format
#  vacc_st_data is an array of hashes, one hash
#  for each state with keys such as dose1 and dose2

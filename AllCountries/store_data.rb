require 'csv'
require 'pp'
require 'matrix'
require 'fileutils'
require_relative '../helper_libs.rb'
require 'time'

country = "Germany"

def parse_time(tstr)
  t=Time.parse(tstr)
  new_t = Time.new(t.year,t.mon,t.day)
  [new_t,t.year,t.mon,t.day]
end

def check_times(all_times)
  sorted_times=all_times.sort
  t1=parse_time(sorted_times[0])
  t2=parse_time(sorted_times[-1])
  ndays = 1+((t2[0].to_i - t1[0].to_i)/86400.0).to_i
  puts "start of report from check_times"
  puts "\s # of unique days \t #{ndays}"
  puts "\s # of dated records \t #{sorted_times.size}"
  puts "end of report from check_times"
  [t1,t2,ndays]
end

def time_idx(time)
  (time.to_i - BASE_TIME)/86400
end

arr=[]
i=0
CSV.foreach("tdata.05-14-2021") do |row|
  i+=1
  next if i==1
  arr << row if row[1]==country
end
abort "country not found" if arr.empty?
all_time = arr.map {|v| v[0]}

# here, we see that the number of records is greater than
# expected
t1,t2, ndy = check_times(all_time.uniq)


sdy = (t1[0].to_i - BASE_TIME)/86400
edy = (t2[0].to_i - BASE_TIME)/86400
all_dates=[]
(sdy..edy).to_a.each do |dy|
  all_dates << Time.at(dy*86400+BASE_TIME).strftime("%Y,%-m,%-d")
end

nprfs=1
state_data = Matrix.build(nprfs,ndy) {|r,c| [["2020,1,1"],[""]]}

(0..ndy-1).to_a.each do |idy|
  (0..nprfs-1).to_a.each do |ipr|
    state_data[ipr,idy][0] = all_dates[idy]
    state_data[ipr,idy][1] = ["0,0,0"]
  end
end


# open files to write out data per prefecture
out_dir = "./CSV"
FileUtils.mkdir_p(out_dir)

File.open(File.join(out_dir,"start_end.day"),"w") do |fout|
  fout.puts sdy, edy
end

outfil_handles=[]
outfil_handles << File.open("#{File.join(out_dir,make_slug(country))}.csv","w")

outfil_handles.each do |of|
  of.puts "year,month,day,positive,death,recovered"
end


arr.each do |varr|
  idy = time_idx(parse_time(varr[0]).first)
  idy = idy-sdy
  value = varr
  pos = value[2].to_i
  rec = value[3].to_i
  ded = value[4].to_i
  data = [pos,ded,rec].join(",")
  state_data[0,idy][1] = data
end


outfil_handles.each_with_index do |of,iprf|
  (0..ndy-1).to_a.each do |idy|
    of.puts (state_data[iprf,idy].flatten).join(",")
  end
end

p "Finished writing data"
outfil_handles.each {|of| of.close}


require 'fileutils'
require 'digest/sha1'
tdir = "/home/saji/VaccData/india_2020_web/src/images/countries"
tim = Time.now.to_i
Dir.glob("*.eps").each do |fil|
  bnam = File.basename(fil,".eps")
  hnam = Digest::SHA1.hexdigest(bnam+""+tim.to_s)
  hnam = hnam.split("").sample(10).join
  ofil = File.join tdir, bnam+"_#{hnam}.png"
  FileUtils.rm(Dir.glob(File.join(tdir,bnam+"*.png")),:verbose=>true)
  cmd = "convert -trim -density 300 -antialias -resize '800' #{fil} #{ofil}"
  puts `echo #{cmd}`
  puts `#{cmd}`
end

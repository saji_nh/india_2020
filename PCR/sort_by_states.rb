require 'json'
require 'pp'
require 'matrix'
require 'fileutils'
require_relative '../helper_libs.rb'
require_relative './states.rb'
require 'time'


# Note - the data has as of 5-May-2021 27 extra days than
# expected of a daily record. This may be because there
# are duplicate records in a single day.
# My approach would be to store the latest data instead
# of performing complicated validations. Since a possibly
# 27 out of 460 records are suspect, the risk of injecting
# bad data is 1/25


def parse_time(tstr)
  t=Time.parse(tstr)
  new_t = Time.new(t.year,t.mon,t.day)
  [new_t,t.year,t.mon,t.day]
end

def check_times(all_times)
  sorted_times=all_times.sort
  t1=parse_time(sorted_times[0])
  t2=parse_time(sorted_times[-1])
  ndays = ((t2[0].to_i - t1[0].to_i)/86400.0).to_i
  puts "start of report from check_times"
  puts "\s # of unique days \t #{ndays}"
  puts "\s # of dated records \t #{sorted_times.size}"
  puts "end of report from check_times"
  [t1,t2,(ndays+1)]
end

def time_idx(time)
  (time.to_i - BASE_TIME)/86400
end

arr=""
File.open("mohfw.json") do |file|

  arr = JSON.load(file)
end
vacc_arr=arr["rows"]
all_time = vacc_arr.map {|v| v["value"]["report_time"]}
all_states = states

# here, we see that the number of records is greater than
# expected
t1,t2, ndy = check_times(all_time.uniq)
sdy = (t1[0].to_i - BASE_TIME)/86400
edy = (t2[0].to_i - BASE_TIME)/86400
state_names = states.values
nprfs = state_names.size
all_dates=[]
(sdy..edy).to_a.each do |dy|
  all_dates << Time.at(dy*86400+BASE_TIME).strftime("%Y,%-m,%-d")
end

state_data = Matrix.build(nprfs,ndy) {|r,c| [["2020,1,1"],[""]]}

(0..ndy-1).to_a.each do |idy|
  (0..nprfs-1).to_a.each do |ipr|
    state_data[ipr,idy][0] = all_dates[idy]
    state_data[ipr,idy][1] = ["0,0,0"]
  end
end


# open files to write out data per prefecture
out_dir = "./ByState"
FileUtils.mkdir_p(out_dir)

File.open(File.join(out_dir,"start_end.day"),"w") do |fout|
  fout.puts sdy, edy
end

File.open(File.join(out_dir,"states.csv"),"w") do |fout|
  fout.puts state_names
end
outfil_handles=[]
state_names.each do |st_name|
  outfil_handles << File.open("#{File.join(out_dir,st_name)}.csv","w")
end

outfil_handles.each do |of|
  of.puts "year,month,day,positive,death,recovered"
end

#vacc_arr.map {|v| v["value"]["report_time"]}

vacc_arr.each do |varr|
  idy = time_idx(parse_time(varr["value"]["report_time"]).first)
  idy = idy-sdy
  value = varr["value"]
  ipr =  states.keys.index(value["state"])
  next if !ipr
  pos = value["confirmed"]
  ded = value["death"]
  rec = value["cured"]
  data = [pos,ded,rec].join(",")
  state_data[ipr,idy][1] = data
end

outfil_handles.each_with_index do |of,iprf|
  (0..ndy-1).to_a.each do |idy|
    of.puts (state_data[iprf,idy].flatten).join(",")
  end
end

p "Finished writing data"
outfil_handles.each {|of| of.close}


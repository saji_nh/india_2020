BASE_TIME = Time.new(2020,1,1).to_i
def to_time(arr)
  y,m,d=arr[0..2]
  (Time.new(y,m,d).to_i - BASE_TIME)/(86400)
end
def make_slug(label)
  label.split(" ").join("_").downcase.gsub("&","and")[0..10]
end

# India_2020

Collecting and analysing various data from India since 2020

## Sources of data:

1. Vaccination data by state (daily) - Mygov.in https://www.mygov.in/sites/default/files/covid/vaccine/covid_vaccine_timeline.json?#{itim}, where itim = Time.now.to_i
2. PCR data (positives, recovered, deaths) - https://github.com/datameet/covid19/tree/master/data
3. Population data  https://data.gov.in/resources/state-wise-population-decadal-population-growth-rate-and-population-density-2011-0
